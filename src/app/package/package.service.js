(function() {
  'use strict';

  angular
      .module('workspace')
      .service('PackageService', PackageService);

  /** @ngInject */
  function PackageService($http, localStorageService) {


    this.getPackage = getPackage;
    this.getPackagesInStore = getPackagesInStore;
    this.setPackagesInStore = setPackagesInStore;
    this.clearAllPackagesInStore = clearAllPackagesInStore;
    
    function getPackage(packageId) {
      return $http.get("https://correios-api-washingtonsoares.c9users.io/package/" + packageId);
    }
    
    function getPackagesInStore(){
      return localStorageService.get('packages');
    }
    
    function setPackagesInStore(packages){
      return localStorageService.set('packages', packages);
    }
    
    function clearAllPackagesInStore(){
      return localStorageService.clearAll();
    }
    
  }

})();
