(function() {
  'use strict';

  angular
    .module('workspace')
    .controller('MyPackageController', MyPackageController);

  /** @ngInject */
  function MyPackageController(PackageService, $log) {
    var vm = this;
    vm.favoritePackages = [];

    init();

    function init() {
      vm.favoritePackages = PackageService.getPackagesInStore();
      $log.log(vm.favoritePackages);
    }
    

 
  }
})();
