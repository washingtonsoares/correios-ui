(function() {
  'use strict';

  angular
    .module('workspace')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController(PackageService, $log, ngNotify, $uibModal) {
    var vm = this;

    vm.package = {};
    vm.getPackage = getPackage;
    vm.getPackagePromise = undefined;
    vm.favoritePackage = favoritePackage;
    
    init();

    function init() {
 
    }
    function getPackage(packageId){
        vm.getPackagePromise = PackageService.getPackage(packageId);
        vm.getPackagePromise.then(
            function(response){
                delete vm.getPackagePromise;
                vm.package = response.data;
            },function(){
                delete vm.getPackagePromise;
                vm.package = {};
                ngNotify.set('Ops! Houve um erro ao buscar essa encomenda!', 'error');
            });
    }
    
    function favoritePackage(favoritePackage){
          var modalInstance = $uibModal.open({
            templateUrl: 'app/main/favoritePackageModal/favorite-package.modal.html',
            resolve: {
              favoritePackage: function () {
                return favoritePackage;
              }
            },
            controller: 'favoritePackageModalController as modal'
          });

          modalInstance.result.then(function (favoritePackage) {
            try{
              var packages = PackageService.getPackagesInStore() || [];
              packages.unshift(favoritePackage);
              PackageService.setPackagesInStore(packages);  
              ngNotify.set('Encomenda salva com sucesso!', 'success');
            }catch(e){
              ngNotify.set('Ops! Houve um erro ao salvar essa encomenda!', 'error');
            }
            
            
                      
          });
          
    }
  
  
  }
})();
