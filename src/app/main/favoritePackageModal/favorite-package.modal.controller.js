(function() {
  'use strict';

  angular
    .module('workspace')
    .controller('favoritePackageModalController', favoritePackageModalController);

  /** @ngInject */
  function favoritePackageModalController(favoritePackage, $uibModalInstance, $log) {
    var vm = this;

    vm.favoritePackage = favoritePackage;
    $log.log(favoritePackage);
    vm.save = save;
    vm.exit = exit;
    
    function save(favoritePackage){
      $uibModalInstance.close(favoritePackage);
    }
    
    function exit(){
      $log.log('caiu');
      $uibModalInstance.dismiss();
    }
  
  }
})();
