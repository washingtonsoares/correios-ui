(function() {
  'use strict';

  angular
    .module('workspace')
    .controller('SingupController', SingupController);

  /** @ngInject */
  function SingupController($log, $auth, ngNotify, $state) {
    var vm = this;

    vm.registrationForm = {};
    vm.submitRegistration = submitRegistration;
    
    function submitRegistration(user){
      $log.info(user);
      $auth.submitRegistration(user)
        .then(function(resp) {
            $log.info(resp);
            ngNotify.set('Verifique sua caixa de entrada e confirme seu cadastro!', 'info');
            $state.go('main');
        })
        .catch(function(resp) {
            $log.info(resp);
        });
    }
 

  
  }
})();
