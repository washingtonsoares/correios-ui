(function() {
  'use strict';

  angular
    .module('workspace')
    .controller('SigninController', SigninController);

  /** @ngInject */
  function SigninController($log, $auth, ngNotify, $state, SessionService) {
    var vm = this;

    vm.loginForm = {};
    vm.submitLogin = submitLogin;
    
    function submitLogin(user){
      $log.info(user);
      $auth.submitLogin(user)
        .then(function(resp) {
            $log.info(resp);
            ngNotify.set('Sucesso!', 'success');
            $state.go('my-packages');
            SessionService.create(resp);
            $log.log(SessionService);
        })
        .catch(function(resp) {
            $log.info(resp);
            ngNotify.set('E-mail ou senha inválidos!', 'error');
        });
    }
 
  
  }
})();
