(function() {
  'use strict';

  angular
    .module('workspace')
    .value('cgBusyDefaults',{
      message:'Carregando',
      backdrop: true,
      templateUrl: 'app/components/cg-busy/custom_template.html'
    });
   

})();
