(function() {
  'use strict';

  angular
      .module('workspace')
      .service('SessionService', SessionService);

  /** @ngInject */
  function SessionService() {

    this.create = create;
    this.get = get;
    
    var email =  undefined;
    var id =  undefined;
    var image =  undefined;
    var name =  undefined;
    var nickname =  undefined;
    var provider =  undefined;
    var uid =  undefined;
    
    function create(session) {
        email = session.email;
        id = session.id;
        image = session.image;
        name = session.name;
        nickname = session.nickname;
        provider = session.provider;
        uid = session.uid;
    }
    
    function get(){
        return {
            email: email,
            id: id,
            image: image,
            name: name,
            nickname: nickname,
            provider: provider,
            uid: uid
        }
    }
    
  }

})();
