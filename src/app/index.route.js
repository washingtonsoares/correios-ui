(function() {
  'use strict';

  angular
    .module('workspace')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('singup', {
        url: '/singup',
        templateUrl: 'app/auth/singup/singup.html',
        controller: 'SingupController',
        controllerAs: 'singup'
      })
      .state('signin', {
        url: '/signin',
        templateUrl: 'app/auth/signin/signin.html',
        controller: 'SigninController',
        controllerAs: 'signin'
      })
      .state('my-packages', {
        url: '/my-packages',
        templateUrl: 'app/package/my-packages.html',
        controller: 'MyPackageController',
        controllerAs: 'myPackages'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
