(function() {
  'use strict';

  angular
    .module('workspace', 
    ['ngAnimate', 
    'ngCookies', 
    'ngTouch', 
    'ngSanitize', 
    'ngMessages', 
    'ngAria', 
    'ui.router', 
    'ui.bootstrap', 
    'toastr',
    'cgBusy',
    'ngNotify',
    'ng-token-auth',
    'angular-loading-bar',
    'LocalStorageModule'
    ]);

})();
