(function() {
  'use strict';

  angular
    .module('workspace')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastrConfig, $authProvider, cfpLoadingBarProvider, localStorageServiceProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;
    
     $authProvider.configure({
        apiUrl: 'https://correios-api-washingtonsoares.c9users.io'
     });
     
     cfpLoadingBarProvider.includeSpinner = false;
     localStorageServiceProvider.setPrefix('myPackage');
  }

})();
